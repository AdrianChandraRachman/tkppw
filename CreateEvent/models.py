from django.db import models

# Create your models here.
class Event(models.Model) :
    Picture = models.ImageField(null = True, blank=True)
    eventName = models.CharField(max_length=80)
    description = models.CharField(max_length=250)
    eventDate = models.DateField()
    tempat = models.CharField(max_length=80)

    def __str__(self):
        return self.eventName
