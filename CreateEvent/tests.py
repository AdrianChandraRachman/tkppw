from django.test import TestCase, Client
from django.urls import resolve,  reverse
from . import models
from .models import Event
from .views import CreateEvent,ViewEvent
from django.core.files.uploadedfile import SimpleUploadedFile
from CreateEvent.models import Event
import datetime
from story.settings import BASE_DIR as path
import os

# Create your tests here.
class TkPpwTest(TestCase):

    # Setup Databases
    def setUp(self):
        newPhoto = SimpleUploadedFile(name='test_image.jpg' , content=open(os.path.join(path, "CreateEvent/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        Event.objects.create(Picture=newPhoto, eventName = 'Farah Fest',description = 'Live Coding from Ragunan',eventDate = datetime.datetime.now(),tempat = 'SKO Ragunan')
      
    

    # Check Databases
    def test_check_amount_createevent_database(self):
        self.assertEqual(len(Event.objects.all()), 1)


    # Check URLs
    def test_create_event_url_exist(self):
        response = Client().get("/CreateEvent/")
        self.assertEqual(response.status_code, 200)

    def test_list_event_url_exist(self):
        response = Client().get("/CreateEvent/view/")
        self.assertEqual(response.status_code, 200)


    # Check Views
    def test_create_event_views_exist(self):       
        newPhoto = SimpleUploadedFile(name='test_image.jpg' , content=open(os.path.join(path, "CreateEvent/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        response = Client().post(reverse("CreateEvent:indexCreate")
                                 , data={"Picture" : newPhoto, "eventName": "Sandi", "description" : "Live" ,"eventDate" :  "12/12/2020", "tempat": "home"}, follow=True)
        self.assertEqual(response.status_code, 200)
        
    def test_list_event_views_exist(self):
        response = Client().get(reverse("CreateEvent:ViewEvent"))
        self.assertEqual(response.status_code, 200)

     

    def test_content_on_html(self):
        newPhoto = SimpleUploadedFile(name='test_image.jpg' , content=open(os.path.join(path, "CreateEvent/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        Event.objects.create(Picture=newPhoto, eventName = 'Farah Fest',description = 'Live Coding from Ragunan',eventDate = '2020-02-20',tempat = 'RUANG')
        response = Client().get('/CreateEvent/view/')
        html_response = response.content.decode('utf8')
        self.assertIn('https://res.cloudinary.com',html_response)
        self.assertIn('Farah Fest',html_response)
        self.assertIn('Live Coding from Ragunan',html_response)
        self.assertIn('Feb. 20, 2020',html_response)
        self.assertIn('RUANG',html_response)
        


    # Checking Templates
    def test_create_event_templates_exist(self):
        response = Client().get("/CreateEvent/")
        self.assertTemplateUsed(response, "indexCreate.html")
    def test_list_event_templates_exist(self):
        response = Client().get("/CreateEvent/view/")
        self.assertTemplateUsed(response, "listEvent.html")



    
    # Check Form Exist & Work
    def test_saving_a_POST_create_event_request(self):
        newPhoto = SimpleUploadedFile(name='test_image.jpg' , content=open(os.path.join(path, "CreateEvent/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        response = Client().post("/CreateEvent/", data={"Picture" : newPhoto, "eventName": "Sandi", "description" : "Live" ,"eventDate" :  "12/12/2020", "tempat": "home"})
        amount = models.Event.objects.filter(eventName="Sandi").count()
        self.assertEqual(amount, 1)


