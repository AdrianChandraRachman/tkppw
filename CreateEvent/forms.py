from django import forms
from django.forms import ModelForm, FileInput
from django.utils.translation import gettext_lazy as _

from .models import Event
class DateInput(forms.DateInput):
    input_type = 'date'

class CreateEventForm(ModelForm) :
    class Meta:
        model = Event
        fields = ['Picture', 'eventName', 'description', 'eventDate','tempat']

        widgets = {
            'Picture' : FileInput(attrs={"id":"imageinput","required":True}),
            'eventName' : forms.TextInput(attrs={"placeholder": "Covid fest", "required": True}),
            'description' : forms.TextInput(attrs={"placeholder": "Live Concert from Home", "required": True}),
            'eventDate': DateInput(),
            'tempat' : forms.TextInput(attrs={"placeholder": "Sabuga", "required": True}),

        }

    # eventName = forms.CharField(label='Event Name', max_length=80,
    #                                 widget=forms.TextInput(attrs={"placeholder": "Covid fest", "required": True}))

    # description  = forms.CharField(label='Description', max_length=250,
    #                                 widget=forms.TextInput(attrs={"placeholder": "Live Concert from Home", "required": True}))
    
    # eventDate = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    # # eventDate = forms.DateTimeField(
    # #     input_formats=['%d/%m/%Y %H:%M'],
    # #     widget=forms.DateTimeInput(attrs={
    # #         'class': 'form-control datetimepicker-input',
    # #         'id' : 'datetimepicker1',
    # #         'data-target': '#datetimepicker1'
    # #     })
    # # )

    # #eventDate  = forms.CharField(label='Event Date', max_length=80,
    #                                 #widget=forms.TextInput(attrs={"placeholder": "10 Januari 2019", "required": True}))
    # tempat  = forms.CharField(label='Location', max_length=80,
    #                                 widget=forms.TextInput(attrs={"placeholder": "Sabuga", "required": True})) 
    