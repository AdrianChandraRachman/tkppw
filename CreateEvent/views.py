from django.shortcuts import render,redirect
from .forms import CreateEventForm
from .models import Event

# Create your views here.
def CreateEvent(request):
    my_form = CreateEventForm()

    if request.method == 'POST':
        my_form = CreateEventForm(request.POST or None ,request.FILES or None)
        
        if my_form.is_valid():
            Event.objects.create(**my_form.cleaned_data)
            return redirect("CreateEvent:indexCreate")

    context = {
        'form' : my_form,
    }
    return render(request, "indexCreate.html", context)   

def ViewEvent(request):
    events = Event.objects.all()

    context = {
        'events' : events
    }
    return render(request, "listEvent.html", context)  

# def model_form_upload(request):
#     if request.method == 'POST':
#         form = DocumentForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = DocumentForm()
#     return render(request, 'model_form_upload.html', {
#         'form': form
#     })
