from django.urls import path

from . import views

app_name = 'Merchandise'

urlpatterns = [
    path('RegisterMerch/<int:id>/', views.registerMerch, name='RegisterMerch'),
    path('ListMerch/<int:id>/', views.displayMerch, name='ListMerch'),
]