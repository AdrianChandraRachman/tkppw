from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .models import Merch
from . import views
from django.core.files.uploadedfile import SimpleUploadedFile
from CreateEvent.models import Event
import datetime
from story.settings import BASE_DIR as path
from django.contrib.auth.models import User
import os




# Create your tests here.
class TkPpwTest(TestCase):
    # Setup Databases
    def setUp(self):
        user = User.objects.create_user(username='aimar',email='aimar@gmail.com', password='aimarpass')
        user.save()
        newPhoto = SimpleUploadedFile(name='test_image.jpg', content=open(os.path.join(path, "Merchandise/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        acara = Event.objects.create(eventName = 'makan-makan',description = 'makan ayam goreng',eventDate = datetime.datetime.now(),tempat = 'Rumah sakit')
        self.barang = Merch.objects.create(event=acara,name='Tot Bag', Price='1.000.000', Picture=newPhoto)


    # Check Databases
    def test_check_amount_registerMerch_database(self):
        self.assertEqual(len(Merch.objects.all()), 1)


    # Check URLs
    def test_register_merch_url_exist(self):
        self.client.login(username='aimar', password='aimarpass')
        response = self.client.get('/Merch/RegisterMerch/1/',{'id':1})
        self.assertEqual(response.status_code, 200)

    def test_list_merch_url_exist(self):
        response = Client().get('/Merch/ListMerch/1/',{'id':1})
        self.assertEqual(response.status_code, 200)



    # Checking Templates
    def test_register_merch_templates_exist(self):
        self.client.login(username='aimar', password='aimarpass')
        response = self.client.get('/Merch/RegisterMerch/1/',{'id':1})
        self.assertTemplateUsed(response, "RegisterMerch.html")

    def test_list_merch_templates_exist(self):
        response = Client().get('/Merch/ListMerch/1/',{'id':1})
        self.assertTemplateUsed(response, "ListMerch.html")

    # check konten website
    def test_content_on_html(self):
        newPhoto = SimpleUploadedFile(name='test_image.jpg', content=open(os.path.join(path, "Merchandise/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        acara = Event.objects.create(eventName = 'makan-makan',description = 'makan ayam goreng',eventDate = datetime.datetime.now(),tempat = 'Rumah sakit')
        Merch.objects.create(event=acara,name='Tot Bag', Price='1.000.000', Picture=newPhoto)
        response = Client().get('/Merch/ListMerch/1/',{'id':1})
        html_response = response.content.decode('utf8')
        self.assertIn('Tot Bag',html_response)
        self.assertIn('1.000.000',html_response)
        self.assertIn("https://res.cloudinary.com",html_response)

     # Tes apakah form ada di HTML response
    def test_merch_form_exists(self):
        self.client.login(username='aimar', password='aimarpass')
        response = self.client.get('/Merch/RegisterMerch/1/',{'id':1})
        self.assertContains(response, "Nama Barang")   

    # Check Form Exist & Work
    def test_saving_a_POST_register_Merch_request(self):
        self.client.login(username='aimar', password='aimarpass')
        amount = models.Merch.objects.all().count()
        newPhoto = SimpleUploadedFile(name='test_image.jpg', content=open(os.path.join(path, "Merchandise/static/images", "photo.png"), 'rb').read(), content_type='image/jpeg')
        response = self.client.post("/Merch/RegisterMerch/1/", data={"event":1,"name": "Pensil", "Price": "1.000.000","Picture":newPhoto})
        self.assertEqual(models.Merch.objects.all().count(), amount + 1)


   #check def __str__
    def test_name_Merchandise(self):
        # merchandise= Merch.objects.create(name='Test Merchandise')
        self.assertEqual(str(self.barang), 'Tot Bag')
