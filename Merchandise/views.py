from django.shortcuts import render,redirect
from .forms import MerchForm
from CreateEvent.models import Event
from .models import Merch
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
# Create your views here.
@login_required(login_url=reverse_lazy('homepage:auth'))
def registerMerch(request, id):
    form = MerchForm()
    if request.method == "POST":
        form = MerchForm(request.POST or None ,request.FILES or None)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.event = Event.objects.get(id=id)
            obj.save()
            return redirect ('/Merch/ListMerch/'+str(id)+'/')
    context = {'form':form,
                  }
    return render(request, 'RegisterMerch.html',context)

def displayMerch(request, id):
    event = Event.objects.get(id=id)
    Merch_list = event.merchandise.all()
    context = { 'event':event,'Merch_list':Merch_list    }
    return render(request, 'ListMerch.html',context)