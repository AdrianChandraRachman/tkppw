from django.forms import ModelForm, TextInput, NumberInput
from .models import RegisterEvent


class RegisterEventForm(ModelForm):
    class Meta:
        model = RegisterEvent
        fields = ["name", "tickets"]
        widgets = {
            "name": TextInput(attrs={"placeholder": "Adrian", "required": True}),
            "tickets": NumberInput(attrs={"placeholder": "2", "required": True})
        }
