from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import RegisterEvent
from .views import registerEvent, displayRegisterEvent
from CreateEvent.models import Event
import datetime

# Create your tests here.
class TkPpwTest(TestCase):

    # Setup Databases
    def setUp(self):
        self.event = Event.objects.create(eventName="Compfest", description="it's fun"
                                          , eventDate=datetime.date(2020, 5, 6), tempat="Balairung UI")
        RegisterEvent.objects.create(event=self.event, name="Adrian", tickets=2)


    # Check Databases
    def test_check_amount_registerevent_database(self):
        self.assertEqual(len(RegisterEvent.objects.all()), 1)


    # Check URLs
    def test_register_event_url_exist(self):
        response = Client().get("/RegisterEvent/1/", {"id":1})
        self.assertEqual(response.status_code, 200)


    def test_display_url_exist(self):
        response = Client().get("/RegisterEvent/display/1/", {"id":1})
        self.assertEqual(response.status_code, 200)


    # Check Views
    def test_register_event_views_exist(self):
        response = Client().post(reverse("RegisterEvent:RegisterEvent", kwargs={'id': 1})
                                 , data={"event": self.event, "name": "Beidou", "tickets": 2}, follow=True)
        self.assertEqual(response.status_code, 200)


    def test_display_views_exist(self):
        response = Client().get(reverse("RegisterEvent:DisplayRegisterEvent", kwargs={'id': 1}))
        self.assertEqual(response.status_code, 200)


    # Checking Templates
    def test_register_event_templates_exist(self):
        response = Client().get("/RegisterEvent/1/", {"id":1})
        self.assertTemplateUsed(response, "RegisterEvent/index.html")


    def test_display_templates_exist(self):
        response = Client().get("/RegisterEvent/display/1/", {"id":1})
        self.assertTemplateUsed(response, "RegisterEvent/displayRegisterEvent.html")


    # Check if HTML display models
    def test_if_HTML_display_models(self):
        response = Client().get("/RegisterEvent/display/1/")
        html_response = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Adrian", html_response)


    # Check Form Exist & Work
    def test_saving_a_POST_register_event_request(self):
        response = Client().post("/RegisterEvent/1/", data={"event": self.event, "name": "Pablo", "tickets": 2})
        amount = RegisterEvent.objects.filter(name="Pablo").count()
        self.assertEqual(amount, 1)


    #check def __str__
    def test_name_RegisterEvent(self):
        obj = RegisterEvent.objects.create(event=self.event, name="Venti", tickets=100)
        self.assertEqual(str(obj), 'Venti')
