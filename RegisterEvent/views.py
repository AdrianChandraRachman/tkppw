from django.shortcuts import render,redirect
from .forms import RegisterEventForm
from .models import RegisterEvent
from CreateEvent.models import Event

# Create your views here.

def registerEvent(request, id):
    form = RegisterEventForm()
    if request.method == "POST":
        form = RegisterEventForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.event = Event.objects.get(id=id)
            obj.save()
            return redirect("CreateEvent:ViewEvent")

    context = {"form": form}
    return render(request, "RegisterEvent/index.html", context)


def displayRegisterEvent(request, id):
    event = Event.objects.get(id=id)
    registerevent = RegisterEvent.objects.all()
    context = {'event': event, 'registerevent': registerevent}
    return render(request, "RegisterEvent/displayRegisterEvent.html", context)
