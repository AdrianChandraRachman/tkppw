from django.urls import path
from . import views

app_name = 'RegisterEvent'

urlpatterns = [
    path('<int:id>/', views.registerEvent, name="RegisterEvent"),
    path('display/<int:id>/', views.displayRegisterEvent, name="DisplayRegisterEvent")
]