from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login,logout,authenticate
from django.contrib import messages
from .forms import RegisterForm

# Create your views here.

def home(request):
    return render(request, 'main.html')

def user_type(request):
    return render(request, 'user_type.html')

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account Created for {username}!')
            return redirect('homepage:auth')
    else:
        form = RegisterForm()
    context = {'form':form}
    return render(request,'register.html',{'form':form})


