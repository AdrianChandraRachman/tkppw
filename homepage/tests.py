from django.test import TestCase, Client
from django.urls import resolve
from .views import home, user_type

# Create your tests here.

class TkPpwTest(TestCase):

    # Check URLs
    def test_home_url_exist(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_user_type_url_exist(self):
        response = Client().get("/user-type/")
        self.assertEqual(response.status_code, 200)

    # Check Views
    def test_home_views_exist(self):
        response = Client().get("")
        self.assertIn("Introduce your business to potential stakeholders during this pandemic", response.content.decode("utf8"))

    def test_user_type_comments_views_exist(self):
        found = resolve("/user-type/")
        self.assertEqual(found.func, user_type)

    # Checking Templates
    def test_home_templates_exist(self):
        response = Client().get("")
        self.assertTemplateUsed(response, "main.html")

    def test_user_type_templates_exist(self):
        response = Client().get("/user-type/")
        self.assertTemplateUsed(response, "user_type.html")

