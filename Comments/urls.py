from django.urls import path
from . import views

app_name = 'Comments'

urlpatterns = [
    path('<int:id>/', views.comments, name='Comments'),
    path('display/<int:id>/', views.displayComments, name='DisplayComments')
]