from django.db import models
from CreateEvent.models import Event
from django.contrib.auth.models import User
# Create your models here.

class Comments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    comment = models.CharField(max_length=500)

    is_checked = models.BooleanField(default=False)

    def __str__(self):
        return self.name
