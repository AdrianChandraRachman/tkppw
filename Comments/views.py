from django.shortcuts import render,redirect
from .forms import CommentsForm
from .models import Comments
from django.contrib import messages
from CreateEvent.models import Event
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy

# Create your views here.

# def comments(request, id):
#     form = CommentsForm()
#     if request.method == 'POST':
#         form = CommentsForm(request.POST)
#         if form.is_valid():
#             obj = form.save(commit=False)
#             obj.event = Event.objects.get(id=id)
#             form.save()
#             return redirect("CreateEvent:ViewEvent")
#     context = {'form': form}
#     return render(request, 'Comments/index.html', context)


def displayComments(request, id):
    unfiltered_comments = Comments.objects.all()
    confirmed = []
    unconfirmed = []
    for i in unfiltered_comments:
        if i.is_checked:
            confirmed.append(i)
        else:
            unconfirmed.append(i)

    context = {
                'confirmed': confirmed,
                'unconfirmed' : unconfirmed,
    }

    event = Event.objects.get(id=id)
    commentsValue = Comments.objects.all()
    context = {'event': event, 'commentsValue': commentsValue}

    return render(request, 'Comments/displayComments.html', context)

@login_required(login_url=reverse_lazy('homepage:auth'))
def comments(request, id):
    form = CommentsForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.event = Event.objects.get(id=id)
        messages.success(request, f"Comment Sucesfully Added")
        form.instance.event_id = id
        form.instance.user = request.user
        form.save()
        return redirect("CreateEvent:ViewEvent")
    context = {'form':form}
    return render(request, 'Comments/index.html', context)


# def comments(request, id):
#     form = CommentsForm()
#     if request.method == 'POST':
#         form = CommentsForm(request.POST)
#         if form.is_valid():
#             obj = form.save(commit=False)
#             obj.event = Event.objects.get(id=id)
#             form.save()
#             return redirect("CreateEvent:ViewEvent")
#     context = {'form': form}
#     return render(request, 'Comments/index.html', context)