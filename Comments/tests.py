from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Comments
from .views import comments, displayComments
from CreateEvent.models import Event
import datetime

# Create your tests here.
class TkPpwTest(TestCase):

    # Setup Databases
    def setUp(self):
        self.event = Event.objects.create(eventName="The International", description="Seru",
                                          eventDate=datetime.date(2020, 2, 4), tempat="Balairung UI")
        Comments.objects.create(event=self.event, name="Eros", comment="Keren bat pars")


    # Check Databases
    def test_check_amount_comments_database(self):
        self.assertEqual(len(Comments.objects.all()), 1)


    # Check URLs
    def test_comments_url_exist(self):
        response = Client().get("/Comments/1/", {"id":1})
        self.assertEqual(response.status_code, 200)


    def test_display_url_exist(self):
        response = Client().get("/Comments/display/1/", {"id":1})
        self.assertEqual(response.status_code, 200)


    # Check Views
    def test_comments_views_exist(self):
        response = Client().post(reverse("Comments:Comments", kwargs={'id': 1})
                                 , data={"event": self.event, "name": "Childe", "comment": "Keep your eyes wide open!"}, follow=True)
        self.assertEqual(response.status_code, 200)


    def test_display_comments_views_exist(self):
        response = Client().get(reverse("Comments:DisplayComments", kwargs={'id': 1}))
        self.assertEqual(response.status_code, 200)


    # Checking Templates
    def test_comments_templates_exist(self):
        response = Client().get("/Comments/1/", {"id":1})
        self.assertTemplateUsed(response, "Comments/index.html")


    def test_display_templates_exist(self):
        response = Client().get("/Comments/display/1/", {"id":1})
        self.assertTemplateUsed(response, "Comments/displayComments.html")


    # Check if HTML display models
    def test_if_HTML_display_models(self):
        response = Client().get("/Comments/display/1/")
        html_response = response.content.decode("utf8")
        self.assertEqual(response.status_code, 200)
        self.assertIn("Keren bat pars", html_response)


    # Check Form Exist & Work
    def test_saving_a_POST_comments_request(self):
        response = Client().post("/Comments/1/", data={"event": self.event, "name": "Escaban", "comment": "Keren bat pars"})
        amount = Comments.objects.filter(name="Escaban").count()
        self.assertEqual(amount, 1)

    # check def __str__
    def test_name_Comments(self):
        obj = Comments.objects.create(event=self.event, name="Diluc", comment="The legend of the \"Darknight Hero\"? An interesting rumor.")
        self.assertEqual(str(obj), "Diluc")
